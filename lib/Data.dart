class Data{
    int id;
    String name;
    String species;
    String status;
    String image;
    Data(this.id, this.name, this.species, this.status, this.image);
}