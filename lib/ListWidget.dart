import 'package:flutter/material.dart';
import 'package:flutter_app2/Data.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ListWidget extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return ListWidgetState();
  }
}

class ListWidgetState extends State<ListWidget>{

  String next = "https://rickandmortyapi.com/api/character?page=1";
  String prev;
  bool parse = false;
  List<Data> listData = [
    // Results(1, "Test Men", "Human", "Alive"),
    // Results(2, "Test Men", "Human", "Alive"),
    // Results(3, "Test Men", "Human", "Alive"),
    // Results(4, "Test Men", "Human", "Alive"),
    // Results(5, "Test Men", "Human", "Alive"),
    // Results(6, "Test Men", "Human", "Alive"),
    // Results(7, "Test Men", "Human", "Alive"),
    // Results(8, "Test Men", "Human", "Alive"),
    // Results(9, "Test Men", "Human", "Alive"),
    // Results(10, "Test Men", "Human", "Alive")
  ];
  var _controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          // You're at the top.
        } else {
          if (next != null && !parse) {
            parse = true;
            _loadList();
          }
        }
      }
    });
    return Scaffold(
        body: Container(
            child: ListView(
              children: _getList(),
              controller: _controller,
            )),
    );
  }

  _loadList() async{
    final response = await http.get(next);
    if (response.statusCode == 200){
      next = (json.decode(response.body) as Map)['info']['next'];
      prev = (json.decode(response.body) as Map)['info']['prev'];
      var listResponseData = (json.decode(response.body) as Map)['results'] as List<dynamic>;
      parse = false;
      setState(() {
        listData.addAll(listResponseData.map((map) => Data(map['id'], map['name'], map['species'], map['status'], map['image'])).toList());
      });
    }
  }

  List<Widget> _getList() {
    return listData.map((Data res) => ListTile(
      title: Text(res.name),
      subtitle: Text(res.species),
      leading: CircleAvatar(
          radius: 20,
          backgroundImage: NetworkImage(res.image)
      ),
      trailing: Text(res.status),
    )).toList();
  }

  @override
  void initState() {
    _loadList();
  }
}