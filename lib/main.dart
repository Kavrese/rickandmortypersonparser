import 'package:flutter/material.dart';
import 'package:flutter_app2/ListWidget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "RickAndMortyPersonParser",
      theme: ThemeData(
        primarySwatch: Colors.blue
      ),
      home: ListWidget(),
    );
  }

}
